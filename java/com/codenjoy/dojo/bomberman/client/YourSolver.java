package com.codenjoy.dojo.bomberman.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.codenjoy.dojo.bomberman.model.BoardAnalyzer;
import com.codenjoy.dojo.bomberman.model.BombTimer;
import com.codenjoy.dojo.bomberman.model.Elements;
import com.codenjoy.dojo.bomberman.model.EventListener;
import com.codenjoy.dojo.bomberman.model.Explodable;
import com.codenjoy.dojo.bomberman.model.Чьототам;
import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;
import com.codenjoy.dojo.services.RandomDice;

import java.awt.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.stream.Collectors;


/**
 * User: Dmytro_Gromovyi
 */
public class YourSolver implements Solver<Board> {

	final private static String URL = "https://dojorena.io/codenjoy-contest/board/player/8t9ks3eml6hf5x7w51be?code=4271050210569572388";
	private static final int BLAST_RANGE = 4;
    private Dice dice;
    private Board board;
    private BoardAnalyzer boardAnalyzer;
    private BombTimer bombTimer;
    private EventListener eventListener;
    private Чьототам чьототам;

    private Queue<Point> previousPoints = new ArrayDeque<>();
	private int ticksToBlow;
	private boolean useBot = true;

	public YourSolver(Dice dice) {
        this.dice = dice;
        this.boardAnalyzer = new BoardAnalyzer();
        this.bombTimer = new BombTimer();
        this.чьототам = new Чьототам();
        this.eventListener = new EventListener();
        this.чьототам.registerEventListener(this.eventListener);
    }

    private class CustomPoint
	 {
	 	Elements element;
	 	Point point;
	 	Direction direction;

		 CustomPoint(Elements element, Point point, Direction direction)
		 {
			 this.element = element;
			 this.point = point;
			 this.direction = direction;
		 }
	 }

    // the method which should be implemented
    @Override
    public String get(Board board)
	 {
	 	this.board = board;
		 ticksToBlow = bombTimer.tick();

		 final List<Explodable> explodables = boardAnalyzer.analyze(board);
			String action = "";
		 if (useBot)
		 {
			 action = move(board.getBomberman(), board, explodables);
		 }

		 System.err.println(eventListener.getCurrentCommand());
		 if (eventListener.getCurrentCommand() != null) {
		 	action = eventListener.getCurrentCommand();
		 	if (action.equals(EventListener.SWAP_SOLVER)){
		 		useBot = !useBot;
			}

		 }
		 if (action.contains(Direction.ACT.name()) && ticksToBlow <= 0)
		 {
			 bombTimer.newBomb();
		 }

		 eventListener.tick();
		 return action;
    }

    public static void main(String[] args) {
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
				    URL,
                new YourSolver(new RandomDice()),
                new Board());
    }

	boolean isCurrentPointProbableWillBlast(List<Explodable> explodables, Board board)
	{
		return explodables
				.stream()
				.map(Explodable::getEstimatedPoint)
				.filter(i -> hasOneSameCoord(i, board.getBomberman()))
				.anyMatch(i -> i.distance(board.getBomberman()) <= BLAST_RANGE);
	}

	boolean hasOneSameCoord(Point p1, Point p2)
	{
		return p1.getX() == p2.getX() || p1.getY() == p2.getY();
	}

	boolean isExplodableItemsInRange(Point bomberman, Board board){
		int x = bomberman.getX();
		int y = bomberman.getY();

		List<Elements> elements = new ArrayList<>();
		for (int x1 = x - 3; x1 <= x + 3; x1++){
			if(x1 != x) {
				Elements e = board.getAt(x1, y);
				elements.add(e);
				if (e.equals(Elements.WALL)){
					break;
				}
			}
		}
		for (int y1 = y - 3; y1 <= x + 3; y1++){
			if(y1 != y) {
				Elements e = board.getAt(x, y1);
				elements.add(e);
				if (e.equals(Elements.WALL)){
					break;
				}
			}
		}
		return elements.contains(Elements.DESTROYABLE_WALL) || elements.contains(Elements.OTHER_BOMBERMAN);
	}

    String move(Point bomberman, Board board, List<Explodable> explodables)
	 {
		 int x = bomberman.getX();
		 int y = bomberman.getY();


		 Collection<CustomPoint> points = new ArrayList<>();
		 //initialize points around
		 CustomPoint up = new CustomPoint(board.getAt(x, y+1), new PointImpl(x, y+1), Direction.UP);
		 CustomPoint down = new CustomPoint(board.getAt(x, y-1), new PointImpl(x, y-1), Direction.DOWN);
		 CustomPoint right = new CustomPoint(board.getAt(x+1, y), new PointImpl(x+1, y), Direction.RIGHT);
		 CustomPoint left = new CustomPoint(board.getAt(x-1, y), new PointImpl(x-1, y), Direction.LEFT);

		 points = Arrays.asList(up, down, right, left);

		 dice = new RandomDice();
		 dice.next(points.size());

		 Optional<CustomPoint> res;
		 List<CustomPoint> pointList = points.stream()
				 .filter(this::filterPointAsValid)
				 .filter(a -> validPlaceToGo(bomberman, a))
				 .collect(Collectors.toList());
		 //RANDOMIZE NOT FINDANY
		 Collections.shuffle(pointList);
		 res = pointList.stream().findFirst();
		 //For each point that is valid check if there is way after
		 if (res.isPresent())
		 {
			 previousPoints.add(res.get().point);
			 if(isInCrossRoad(bomberman) && validPlaceToGo(bomberman, res.get()) &&
				 (isExplodableItemsInRange(bomberman, board) || isCurrentPointProbableWillBlast(explodables, board)))
			 {
				return res.get().direction.ACT(true);
			 	//TODO: PLACE BOMB AND HIDE @Taras
			 }
			 return res.get().direction.toString();
		 }
		 else
		 {
		 	//Clear all history points, maybe way back is solution.S
			 previousPoints.clear();
			 res = points.stream()
					 .filter(this::filterPointAsValid)
					 .findAny();
			 if (res.isPresent())
			 {
				 previousPoints.add(res.get().point);
				 return res.get().direction.ACT(true);
			 }
			 //No way available -> stop.
			 else
			 {
				 return Direction.ACT.toString();
			 }
		 }
	 }

	boolean validPlaceToGo(Point curr, CustomPoint pointToGo) {
    	//curr would be as bomb
		//avoid dead ways
		int x = pointToGo.point.getX();
		int y = pointToGo.point.getY();

		CustomPoint up = new CustomPoint(board.getAt(x, y+1), new PointImpl(x, y+1), Direction.UP);
		CustomPoint down = new CustomPoint(board.getAt(x, y-1), new PointImpl(x, y-1), Direction.DOWN);
		CustomPoint right = new CustomPoint(board.getAt(x+1, y), new PointImpl(x+1, y), Direction.RIGHT);
		CustomPoint left = new CustomPoint(board.getAt(x-1, y), new PointImpl(x-1, y), Direction.LEFT);

		Collection<CustomPoint> waysOut = Arrays.asList(up, down ,left, right);
		//Check if there is any way out if placed bomb and dead-end.
		return waysOut.stream()
				.filter(a -> !a.point.equals(curr))
				.filter(a -> !board.getBarriers().contains(a.point))
				.anyMatch(this::filterPointAsValid);
	}

	boolean filterPointAsValid(CustomPoint point)
	 {
	 	Point currentPoint = point.point;
	 	if(point.element.equals(Elements.NONE)
				&& notInBlastZone(currentPoint)
				&& !previousPoints.contains(currentPoint))
		{
			return true;
		}
	 	return false;
	 }


	boolean isInBlastZone(Point point, Board board)
	 {
	 	Collection<Point> blasts = Optional.ofNullable(board.getBlasts()).orElse(Collections.EMPTY_LIST);
	 	Collection<Point> futureBlasts = Optional.ofNullable(board.getFutureBlasts()).orElse(Collections.EMPTY_LIST);
	 	blasts.addAll(futureBlasts);
	 	return blasts.contains(point);
	 }

	 boolean notInBlastZone(Point point)
	 {
	 	return !isInBlastZone(point, board);
	 }

	 boolean isInCrossRoad(Point point)
	 {
	 	 Collection<Point> points;

		 int y = point.getY();
		 int x = point.getX();

		 Point upRight = new PointImpl(x+1, y+1);
		 Point upLeft = new PointImpl(x-1, y+1);
		 Point downRight = new PointImpl(x+1, y-1);
		 Point downLeft = new PointImpl(x-1, y-1);

		 points = Arrays.asList(upRight, upLeft, downLeft, downRight);

		return points.stream().allMatch(a -> board.getAt(a).equals(Elements.WALL));
	 }



}
