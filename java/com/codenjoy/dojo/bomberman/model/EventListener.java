package com.codenjoy.dojo.bomberman.model;

import com.codenjoy.dojo.services.Direction;

import java.awt.event.KeyEvent;


public class EventListener
{
	public static final String SWAP_SOLVER = "SWAP";

	private String currentCommand;

	public void onEvent(KeyEvent keyEvent)
	{
		currentCommand = resolveDirection(keyEvent);
	}

	public void tick() {
		currentCommand = null;
	}

	public String getCurrentCommand()
	{
		return currentCommand;
	}

	private String resolveDirection(KeyEvent keyEvent)
	{
		if (keyEvent.getKeyChar() == 'w')
		{
			return Direction.UP.name();
		}
		else if (keyEvent.getKeyChar() == 's')
		{
			return Direction.DOWN.name();
		}
		else if (keyEvent.getKeyChar() == 'a')
		{
			return Direction.LEFT.name();
		}
		else if (keyEvent.getKeyChar() == 'd')
		{
			return Direction.RIGHT.name();
		}
		else if (keyEvent.getKeyChar() == 'q')
		{
			return SWAP_SOLVER;
		}
		else
		{
			return keyEvent.getKeyChar() == 'f' ? Direction.ACT.name() : "";
		}
	}
}
