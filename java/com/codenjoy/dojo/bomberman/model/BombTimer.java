package com.codenjoy.dojo.bomberman.model;

public class BombTimer
{
	private static final int GENERAL = 5;
	private int countdown;

	public void newBomb()
	{
		countdown = GENERAL;
	}

	// return time to blow, 0 - if bomb has been exploded, negative if there are no placed bombs
	public int tick()
	{
		if (countdown >= 0)
		{
			countdown--;
		}
		return countdown;
	}
}
