package com.codenjoy.dojo.bomberman.model;

import com.codenjoy.dojo.bomberman.model.Elements;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;

public class Explodable {

    private Point currentPoint;
    private Point estimatedPoint;
    //DESTROYABLE_WALL MEAT_CHOPPER DEAD_MEAT_CHOPPER
    private Elements type;
    private Direction direction;

    private final int STEPS = 5;

    public Point getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(Point currentPoint) {
        this.currentPoint = currentPoint;
    }

    public Point getEstimatedPoint() {
        if (direction == null) return new PointImpl();
        if (getDirection().equals(Direction.UP)) {
            estimatedPoint = new PointImpl(currentPoint.getX(), currentPoint.getY() + STEPS);
        } else if (getDirection().equals(Direction.DOWN)) {
            estimatedPoint = new PointImpl(currentPoint.getX(), currentPoint.getY() - STEPS);
        } else if (getDirection().equals(Direction.LEFT)) {
            estimatedPoint = new PointImpl(currentPoint.getX() - STEPS, currentPoint.getY());
        } else if (getDirection().equals(Direction.RIGHT)) {
            estimatedPoint = new PointImpl(currentPoint.getX() + STEPS, currentPoint.getY());
        }
        return estimatedPoint;
    }

    public void setEstimatedPoint(Point estimatedPoint) {
        this.estimatedPoint = estimatedPoint;
    }

    public Elements getType() {
        return type;
    }

    public void setType(Elements type) {
        this.type = type;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Explodable{" +
                "estimatedPoint=" + getEstimatedPoint() +
                '}';
    }
}
