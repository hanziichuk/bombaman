package com.codenjoy.dojo.bomberman.model;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class Чьототам extends JFrame
{
	private EventListener eventListener;

	public Чьототам() {
		setLocationRelativeTo(null);
		setSize(1, 1);
		setVisible(true);
		addKeyListener(new KeyAdapterImpl());
		setDefaultCloseOperation(1);
	}

	public void registerEventListener(EventListener eventListener)
	{
		this.eventListener = eventListener;
	}

	class KeyAdapterImpl extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e)
		{
			eventListener.onEvent(e);
		}
	}

}
