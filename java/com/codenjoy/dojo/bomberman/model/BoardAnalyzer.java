package com.codenjoy.dojo.bomberman.model;

import com.codenjoy.dojo.bomberman.client.Board;
import com.codenjoy.dojo.bomberman.model.Elements;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoardAnalyzer { // ditto kaka

    Board previousBoard = new Board();

    public List<Explodable> analyze(final Board board) {
        if (previousBoard.size() == 0) {
            previousBoard.forString(board.getLayersString().get(0));
            return Collections.emptyList();
        }
        ArrayList<Explodable> explodables = new ArrayList<>();
        board.getMeatChoppers().forEach(mc -> {
            Explodable explodable = new Explodable();
            explodable.setType(Elements.MEAT_CHOPPER);
            explodable.setCurrentPoint(mc);
            explodable.setDirection(getDirection(mc));
            explodables.add(explodable);
        });
        previousBoard.forString(board.getLayersString().get(0));
        return explodables;
    }

    private Direction getDirection(Point point) {
        try {
            if (previousBoard.getAt(point.getX(), point.getY() + 1).equals(Elements.MEAT_CHOPPER)) {
                return Direction.DOWN;
            } else if (previousBoard.getAt(point.getX(), point.getY() - 1).equals(Elements.MEAT_CHOPPER)) {
                return Direction.UP;
            } else if (previousBoard.getAt(point.getX() + 1, point.getY()).equals(Elements.MEAT_CHOPPER)) {
                return Direction.LEFT;
            } else if (previousBoard.getAt(point.getX() - 1, point.getY()).equals(Elements.MEAT_CHOPPER)) {
                return Direction.RIGHT;
            }
        } catch (IllegalArgumentException e) {}
        return null;
    }
}
